<?php

namespace Drupal\charts_text_filter\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a filter to allow charts to be embedded in text.
 *
 * @Filter(
 *   id = "filter_charts_text_filter",
 *   title = @Translation("Charts text filter"),
 *   description = @Translation("Allows charts to be embedded in text."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 * )
 */
class ChartsTextFilter extends FilterBase implements ContainerFactoryPluginInterface {

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a ChartsTextFilter object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RendererInterface $renderer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {

    $dom = Html::load($text);
    $elements = $dom->getElementsByTagName('chart');
    if ($elements->length === 0) {
      return new FilterProcessResult(Html::serialize($dom));
    }

    /** @var \DOMElement $element */
    foreach ($elements as $element) {
      $attributes = [];
      foreach ($element->attributes as $attribute) {
        // If the attribute starts with 'data-chart-series', place it as a new
        // item in $attributes['data-chart-series'].
        if (str_starts_with($attribute->nodeName, 'data-chart-series')) {
          $attributes['data-chart-series'][] = Xss::filter($attribute->nodeValue);
        }
        // If the attribute starts with 'data-chart-data-title', place it as a
        // new item in $attributes['data-chart-data-title'].
        elseif (str_starts_with($attribute->nodeName, 'data-chart-data-title')) {
          $attributes['data-chart-data-title'][] = Xss::filter($attribute->nodeValue);
        }
        // If the attribute starts with 'data-chart-data', place it as a new
        // item in $attributes['data-chart-data'].
        elseif (str_starts_with($attribute->nodeName, 'data-chart-data')) {
          $raw_data = Xss::filter($attribute->nodeValue);
          $data = $this->stringToArray($raw_data);
          // Convert the string values to numbers.
          $data = array_map('floatval', $data);
          $attributes['data-chart-data'][] = $data;
        }
        // If the attribute starts with 'data-chart-yaxis', place it as a new
        // item in $attributes['data-chart-yaxis'].
        elseif (str_starts_with($attribute->nodeName, 'data-yaxis-title')) {
          $attributes['data-yaxis-title'][] = Xss::filter($attribute->nodeValue);
        }
        else {
          $attributes[$attribute->nodeName] = Xss::filter($attribute->nodeValue);
        }
      }
      $id = $attributes['id'] ?? Html::getUniqueId('chart');
      $chart_library = $attributes['data-chart-library'] ?? NULL;
      $chart_type = $attributes['data-chart-type'] ?? 'line';
      $title = $attributes['data-title'] ?? '';
      $chart_series = $attributes['data-chart-series'] ?? [];
      $chart_data_title = $attributes['data-chart-data-title'] ?? [];
      $chart_data = $attributes['data-chart-data'] ?? [];
      $xaxis_title = $attributes['data-xaxis-title'] ?? '';
      $xaxis_labels = $this->stringToArray($attributes['data-xaxis-labels']) ?? [];
      $yaxis_title = $attributes['data-yaxis-title'] ?? [];
      $options = !empty($attributes['data-options']) ? $this->stringToArray($attributes['data-options']) : [];

      $chart = [];
      $chart[$id] = [
        '#type' => 'chart',
        '#chart_type' => $chart_type,
        '#title' => $title ?? '',
        '#raw_options' => $options,
      ];
      if (!empty($chart_library)) {
        $chart[$id]['#chart_library'] = $chart_library;
      }

      // Iterate through the series and add the data to the chart.
      foreach ($chart_series as $key => $value) {
        $chart[$id][] = [
          '#type' => 'chart_data',
          '#title' => $chart_data_title[$key] ?? '',
          '#data' => $chart_data[$key],
        ];
      }
      $chart[$id]['xaxis'] = [
        '#type' => 'chart_xaxis',
        '#title' => $xaxis_title ?? '',
        '#labels' => $xaxis_labels,
      ];
      if (is_countable($yaxis_title) && count($yaxis_title) > 1) {
        foreach ($yaxis_title as $value) {
          $chart[$id]['yaxis'][] = [
            '#type' => 'chart_yaxis',
            '#title' => $value ?? '',
          ];
        }
      }
      elseif (is_countable($yaxis_title) && count($yaxis_title) === 1) {
        $chart[$id]['yaxis'] = [
          '#type' => 'chart_yaxis',
          '#title' => $yaxis_title[0] ?? '',
        ];
      }

      $rendered_chart = $this->renderer->render($chart);
      $wrapper = $dom->createElement('div');
      $tpl = new \DOMDocument();
      $tpl->loadHtml($rendered_chart);
      $wrapper->appendChild($dom->importNode($tpl->documentElement, TRUE));
      $element->parentNode->replaceChild($wrapper, $element);
    }

    return new FilterProcessResult(Html::serialize($dom));
  }

  /**
   * Convert a string written as an array to an array.
   *
   * For example, "['a','b','c']" becomes ['a','b','c'].
   *
   * @param string $string
   *   The string to convert.
   *
   * @return array
   *   The converted array.
   */
  private function stringToArray(string $string): array {
    $string = str_replace(['[', ']', "'"], '', $string);
    return explode(',', $string);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('renderer'));
  }

}
